import utils
import sklearn.svm as svm


def get_support_vectors():
    data = utils.read_file_to_list(filename='svm-data.csv')
    x, y = utils.get_x_y(data)

    classifier = svm.SVC(kernel='linear', C=100000, random_state=241)
    classifier.fit(x, y)
    print "Support vectors indices: ", classifier.support_
    result = utils.get_line_by_arr([i + 1 for i in classifier.support_])
    utils.write_to_file(filename='svm/task1.txt', line=result)
