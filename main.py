import tasks1
import decision_trees as task2
import knn as task3
import knn_regression as task4
import perceptron as task5
import svm_train as task6

if __name__ == '__main__':
    tasks1.make_all_tasks()
    task2.find_important_features()
    task3.find_optimal_k()
    task4.find_optimal_p()
    task5.find_accuracy_diff()
    task6.get_support_vectors()
