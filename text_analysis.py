from sklearn import datasets
from sklearn.svm import SVC
from sklearn.cross_validation import KFold
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.grid_search import GridSearchCV

import numpy as np
import sys
import utils
import copy


print 'Download...'

newsgroups = datasets.fetch_20newsgroups(
        subset='all',
        categories=['alt.atheism', 'sci.space']
)

print 'Done'

x = newsgroups.data
y = newsgroups.target

tf_idf = TfidfVectorizer()
x_transform = tf_idf.fit_transform(x, y)


grid = {'C': np.power(10.0, np.arange(-5, 6))}
cv = KFold(y.size, n_folds=5, shuffle=True, random_state=241)
clf = SVC(kernel='linear', random_state=241)
gs = GridSearchCV(clf, grid, scoring='accuracy', cv=cv)
gs.fit(x_transform, y)

best_c = 10.0 ** -5
best_accuracy = - sys.maxint

for score in gs.grid_scores_:
    print "scope: ", score.mean_validation_score
    if score.mean_validation_score > best_accuracy:
        best_accuracy = score.mean_validation_score
        best_c = score.parameters['C']

print "Best C: ", best_c

best_classifier = SVC(kernel='linear', random_state=241)
best_classifier.fit(x_transform, y)
coefficients = copy.deepcopy(best_classifier.coef_)


def get_max_values_indexes(values, nums):
    result = []

    for i in xrange(nums):
        arg_max = np.argmax(values, axis=0)
        result.append(arg_max)
        values[arg_max] = - sys.maxint

    return result


feature_mapping = tf_idf.get_feature_names()
indexes = get_max_values_indexes(coefficients, 10)

worlds = [feature_mapping[ind] for ind in indexes]
print "Worlds: ", worlds

utils.write_to_file('text_analysis/task1.txt', ' '.join(worlds))

