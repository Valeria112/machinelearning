import pandas
import numpy as np
from collections import Counter
import re
import utils


def task1(data):
    counts = data['Sex'].value_counts()
    result = utils.get_line_by_arr([counts['male'], counts['female']])
    utils.write_to_file('pandas/task1.txt', result)


def task2(data):
    counts = data['Survived'].value_counts()
    result = round(counts[1] / float(counts[0] + counts[1]) * 100, 2)
    utils.write_to_file('pandas/task2.txt', str(result))


def task3(data):
    counts = data['Pclass'].value_counts()
    result = round(counts[1] / float(len(data)) * 100, 2)
    utils.write_to_file('pandas/task3.txt', str(result))


def task4(data):
    ages = [x for x in data['Age'] if not np.isnan(x)]
    mean_age = np.mean(ages, axis=0)
    median_age = np.median(ages, axis=0)
    result = utils.get_line_by_arr([round(mean_age, 2), round(median_age, 2)])
    utils.write_to_file('pandas/task4.txt', result)


def task5(data):
    sib_sp = data['SibSp']
    parch = data['Parch']
    result = round(np.corrcoef(sib_sp, parch)[0, 1], 2)
    utils.write_to_file('pandas/task5.txt', str(result))


def get_sex_and_first_name(name):
    result = re.match(r'.*, (\w+)\. (\w*)', name)
    sex = 0
    fname = ''

    if result:
        prefix = result.group(1)
        fname = result.group(2)

        if prefix == 'Mrs':
            sex = 1
            result = re.match(r'.*\((\w+)', name)
            if result: fname = result.group(1)

    return sex, fname


def task6(data):
    names = data['Name']
    first_names = Counter()

    for name in names:
        sex, fname = get_sex_and_first_name(name)
        if sex == 1: first_names[fname] += 1

    result = max(first_names, key=first_names.get)
    utils.write_to_file('pandas/task6.txt', str(result))


def make_all_tasks():
    data = pandas.read_csv('data/titanic.csv', index_col='PassengerId')
    task1(data)
    task2(data)
    task3(data)
    task4(data)
    task5(data)
    task6(data)
