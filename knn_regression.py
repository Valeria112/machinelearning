import utils
import sklearn.datasets as datasets
import sklearn.cross_validation as cv
import sklearn.neighbors as neighbors
import sklearn.preprocessing as preprocessing
import numpy as np


def knn_regression_cv(x, y, grid):
    cross_validator = cv.KFold(n=len(y), n_folds=5, shuffle=True, random_state=42)
    results = []

    for p in grid:
        regressor = neighbors.KNeighborsRegressor(n_neighbors=5,
                                                  metric='minkowski',
                                                  p=p,
                                                  weights='distance')
        scores = cv.cross_val_score(estimator=regressor,
                                    cv=cross_validator,
                                    X=x, y=y,
                                    scoring='mean_squared_error')
        results.append(np.mean(scores, axis=0))

    return results


def find_optimal_p():
    boston_data = datasets.load_boston()
    sdata = preprocessing.scale(boston_data['data'])
    target = boston_data['target']
    grid = np.linspace(1, 10, num=200)

    result = knn_regression_cv(sdata, target, grid)
    p_ind = np.argmax(result, axis=0) + 1
    p = round(grid[p_ind], 2)
    print("p = %d, result(p) = %d" % (p, result[p_ind]))
    utils.write_to_file('knn_regression/task1.txt', str(p))
