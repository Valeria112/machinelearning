import utils
import numpy as np
import sklearn.metrics as metrics
from sklearn.linear_model import Perceptron
from sklearn.preprocessing import StandardScaler


def learn_perceptron(x, y):
    classifier = Perceptron(random_state=241)
    classifier.fit(x, y)
    return classifier


def test_classifier(classifier, x, y):
    y_predict = classifier.predict(x)
    return metrics.accuracy_score(y, y_predict)


def find_accuracy(data, test_data, scaler=None):
    x, y = utils.get_x_y(data)
    x_t, y_t = utils.get_x_y(test_data)

    if (scaler):
        x = scaler.fit_transform(x)
        x_t = scaler.fit_transform(x_t)

    classifier = learn_perceptron(x, y)
    accuracy = test_classifier(classifier, x_t, y_t)

    return accuracy


def find_accuracy_diff():
    train_data = utils.read_file_to_list(filename='perceptron-train.csv')
    test_data = utils.read_file_to_list(filename='perceptron-test.csv')

    accuracy = find_accuracy(train_data, test_data)
    print("accuracy(non scaler)=%F" %(accuracy))

    scaler = StandardScaler()
    saccuracy = find_accuracy(train_data, test_data, scaler=scaler)
    print("accuracy(scaler)=%F" %(saccuracy))

    diff = round(saccuracy -accuracy, 2)
    utils.write_to_file('perceptron/task1.txt', str(diff))
