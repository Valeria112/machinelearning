import utils
import sklearn.cross_validation as cv
import sklearn.neighbors as neighbors
import sklearn.preprocessing as preprocessing
import numpy as np


def knn_cross_validation(x, y, n_folds=5, shuffle=True, random_state=5):
    cross_validator = cv.KFold(n=len(y), n_folds=n_folds, shuffle=shuffle, random_state=random_state)
    results = []

    for k in xrange(1, 50):
        classifier = neighbors.KNeighborsClassifier(n_neighbors=k)
        scores = cv.cross_val_score(estimator=classifier, cv=cross_validator, X=x, y=y)
        results.append(np.mean(scores, axis=0))

    return results


def find_optimal_k():
    data = utils.read_file_to_list(filename='wine.data')
    x, y = utils.get_x_y(data)

    result = knn_cross_validation(x, y)
    k1 = np.argmax(result, axis=0) + 1
    q1 = round(result[k1 - 1], 2)

    x_scaled = preprocessing.scale(x)
    scaled_result = knn_cross_validation(x_scaled, y)
    k2 = np.argmax(scaled_result, axis=0) + 1
    q2 = round(scaled_result[k2 - 1], 2)

    utils.write_to_file('knn/task1.txt', str(k1))
    utils.write_to_file('knn/task2.txt', str(q1))
    utils.write_to_file('knn/task3.txt', str(k2))
    utils.write_to_file('knn/task4.txt', str(q2))
