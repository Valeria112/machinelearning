import pandas
import numpy as np
from sklearn.tree import DecisionTreeClassifier
import utils
import sys


sex_alias = {"male": 0, "female": 1}
feature_alias = ['Pclass', 'Fare', 'Age', 'Sex']


def filter_data(data):
    X = []
    y = []

    p_class_col = data['Pclass']
    fare_col = data['Fare']
    age_col = data['Age']
    sex_col = [sex_alias[x] for x in data['Sex']]
    survived_col = data['Survived']

    for p_class, fare, age, sex, survived in zip(p_class_col, fare_col, age_col, sex_col, survived_col):
        if not(np.isnan(p_class) or np.isnan(fare) or np.isnan(age) or np.isnan(sex) or np.isnan(survived)):
            X.append([p_class, fare, age, sex])
            y.append(survived)

    return X, y


def find_important_features():
    data = pandas.read_csv('data/titanic.csv', index_col='PassengerId')
    X, y = filter_data(data)

    classifier = DecisionTreeClassifier(random_state=241)
    classifier.fit(X, y)

    features = classifier.feature_importances_
    general = np.argmax(features, axis=0)
    features[general] = - sys.maxint
    sub_general = np.argmax(features, axis=0)

    result = feature_alias[general] + ' ' + feature_alias[sub_general]
    utils.write_to_file('decision_tree/task1.txt', result)

    return features

