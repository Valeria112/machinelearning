def write_to_file(filename, line):
    with open('reports/' + filename, 'w') as file:
        file.write(line)
    file.close()


def get_line_by_arr(array):
    return ' '.join([str(value) for value in array])


def read_file_to_list(filename, delimiter=','):
    data = []
    with open('data/' + filename, 'r') as file:
        for line in file:
            data.append([float(x) for x in line.split(delimiter)])
    file.close()
    return data


def get_x_y(data):
    y = [row[0] for row in data]
    x = [row[1:] for row in data]

    return x, y
